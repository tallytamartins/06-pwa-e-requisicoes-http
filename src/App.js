import React, { useState, useEffect } from 'react'
import './App.css'

function App() {
  const [cocktailName, setCocktailName] = useState('')
  const [cocktailSelect, setCocktailSelect] = useState('')
  const [cocktails, setCocktails] = useState([])

  useEffect(() => {
    if (cocktailSelect === '') {
      setCocktails([])
      return
    }
    //API Search By Select Box
    const searchCocktailBySelect = async () => {
      const req = await fetch(
        `https://thecocktaildb.com/api/json/v1/1/${cocktailSelect}.php`
      )
      const json = await req.json()
      setCocktails(json.drinks)
      setCocktailName('')
    }
    searchCocktailBySelect()
  }, [cocktailSelect])

  // API Search By Name
  const searchCocktailByName = async () => {
    if (cocktailName === '') {
      setCocktails([])
      return
    }
    const req = await fetch(
      `https://thecocktaildb.com/api/json/v1/1/search.php?s=${cocktailName}`
    )
    const json = await req.json()
    setCocktails(json.drinks)
    setCocktailSelect('')
  }

  return (
    <div className="container">
      <h1>Welcome to the drinks website!</h1>
      <br />
      <h2>Search by name:</h2>
      {/* //Pesquisa por nome */}
      <div className="searchByName">
        <input
          type="text"
          onChange={e => setCocktailName(e.target.value)}
          value={cocktailName}
        />
        <button onClick={() => searchCocktailByName()}>Search</button>
      </div>
      <h2>Or Click Here for random drink:</h2>
      {/* Pesquisa pré-estabelecida */}
      <div className="searchDefined">
        <select
          onChange={e => setCocktailSelect(e.target.value)}
          value={cocktailSelect}
        >
          <option value="" key="empty"></option>
          <option value="random" key="random">
            Random Drink
          </option>
        </select>
      </div>
      {cocktails.length > 0 && (
        <div className="cocktails-container">
          {cocktails.map((cocktail, index) => (
            <div key={index}>
              <h3>{cocktail.strDrink}</h3>
              <div className="image-cocktail">
                <img src={cocktail.strDrinkThumb} alt="" />
              </div>
              <h4>Ingredients</h4>
              <ul>
                {cocktail.strIngredient1 && <li>{cocktail.strIngredient1}</li>}
                {cocktail.strIngredient2 && <li>{cocktail.strIngredient2}</li>}
                {cocktail.strIngredient3 && <li>{cocktail.strIngredient3}</li>}
                {cocktail.strIngredient4 && <li>{cocktail.strIngredient4}</li>}
                {cocktail.strIngredient5 && <li>{cocktail.strIngredient5}</li>}
                {cocktail.strIngredient6 && <li>{cocktail.strIngredient6}</li>}
                {cocktail.strIngredient7 && <li>{cocktail.strIngredient7}</li>}
                {cocktail.strIngredient8 && <li>{cocktail.strIngredient8}</li>}
                {cocktail.strIngredient9 && <li>{cocktail.strIngredient9}</li>}
                {cocktail.strIngredient10 && (
                  <li>{cocktail.strIngredient10}</li>
                )}
                {cocktail.strIngredient11 && (
                  <li>{cocktail.strIngredient11}</li>
                )}
                {cocktail.strIngredient12 && (
                  <li>{cocktail.strIngredient12}</li>
                )}
                {cocktail.strIngredient13 && (
                  <li>{cocktail.strIngredient13}</li>
                )}
                {cocktail.strIngredient14 && (
                  <li>{cocktail.strIngredient14}</li>
                )}
                {cocktail.strIngredient15 && (
                  <li>{cocktail.strIngredient15}</li>
                )}
              </ul>
              <h4>How to do</h4>
              <p>{cocktail.strInstructions}</p>
            </div>
          ))}
        </div>
      )}
    </div>
  )
}

export default App
